(defproject tagsview "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [ring "1.3.2"]
                 [hiccup "1.0.5"]
                 [compojure "1.3.1"]
                 [com.novemberain/monger "2.0.0"]]

  :main ^:skip-aot tagsview.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
