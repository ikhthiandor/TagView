(ns tagsview.core
  (:gen-class)
  (:require [ring.adapter.jetty :refer :all]
            [ring.middleware.reload :refer :all]
            [ring.middleware.stacktrace :refer :all]
            [ring.middleware.params :refer :all]
            [hiccup.core :as h]
            [hiccup.page :as page]
            [compojure.core :refer [defroutes GET POST]]
            [compojure.route :as static-route]
            [monger.core :as mg]
            [monger.collection :as mc]
            [clojure.string :as str])
  (:import [org.bson.types ObjectId]))

(defn split-tags [s]
  (str/split s #","))

(defn index []
  (page/html5
    [:head
     [:title "hello world"]
     [:link {:rel "stylesheet" :type "text/css" :href "css/style.css"}]
    ]
    [:body
     [:div {:id "form" }
      [:form {:action "/" :method "POST"}
       [:label "Title: "]
       [:input {:type "text" :id "title" :name "title"}]
       [:br]
       [:br]
       [:label "Link: "]
       [:input {:type "text" :name "link"}]
       [:br]
       [:br]
       [:label "Tags (Use comma to seperate your tags): "]
       [:input {:type "text" :name "tag"}]
       [:br]
       [:br]
       [:input {:type "submit"}]
      ]
     ]
    ]))

(defn tags [taglist]
  (page/html5
    [:head
     [:title "hello world"]
     [:link {:rel "stylesheet" :type "text/css" :href "css/style.css"}]
    ]
    [:body
     [:h2 "This is the Tags Page. It will show all the tags"]
     [:ul
      (for [x taglist]
        [:li x])
     ]
    ]))
(defn links [l]
  (page/html5
    [:head
     [:title "hello world"]
     [:link {:rel "stylesheet" :type "text/css" :href "css/style.css"}]
    ]
    [:body
     [:h2 "Links page"]
     [:ul
      (for [x (range 4)]
        [:li x])
     ]
    ]))

(defn monger-insert [data] (let [conn (mg/connect)
      db   (mg/get-db conn "playground")
      coll "haha"]
      (mc/insert db coll (assoc data :_id (ObjectId.)))))

(defn data-handler [post-data]
  (page/html5
    [:head
     [:title "hello world"]
     [:link {:rel "stylesheet" :type "text/css" :href "css/style.css"}]]
    [:body
     [:h2 "This is the Tags Page. It will show all the tags"]
     [:ul
      (for [[key val] post-data]
        [:li (str post-data)]
        )]]))

(defroutes routes
  (GET "/" [] (index))
  (POST "/" [title link tag] (do 
    (monger-insert {:title title, :link link :tags tag}) 
    (index)))
  (GET "/tags" [] (tags '("tag1" "tag2" "tag3" "tag4")))
  (GET "/links" [] (links "MyLink"))
  (static-route/resources "/" []))

(def app
  (wrap-stacktrace (wrap-reload (wrap-params #'routes) '(tagsview.core))))

(defn boot []
  (run-jetty #'app {:port 3000}))

(defn -main
  "Entry point of the application"
  [& args]
  (boot))
